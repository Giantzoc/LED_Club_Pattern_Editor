<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sequence;
use App\Pattern;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SequenceController extends Controller
{
    // all Sequences
    public function index()
    {
        $Sequences = Sequence::all()->sortByDesc('id')->toArray();
        return array_reverse($Sequences);
    }
 
    // add Sequence
    public function add(Request $request)
    {        
        $Sequence = new Sequence([
            'name' => $request->input('name'),
        ]);
        $Sequence->save();
 
        return response()->json('The Sequence successfully added');
    }
 
    // edit Sequence
    public function edit($id)
    {
        $sequence = Sequence::find($id);
        $patternDropdown = [];
        $patterns = Pattern::all();
        
        foreach($patterns as $pattern){
            array_push($patternDropdown, ["id" => $pattern->id, "name" => $pattern->name]);
        };

        //$steps = $sequence->steps;
        //Log::debug($steps);
        return response()->json([
            'sequence' => $sequence,
            'steps' => $sequence->steps()->get(),
            'patterns' => $patternDropdown
            ]
        );
    }
 
    // update Sequence
    public function update($id, Request $request)
    {
        $Sequence = Sequence::find($id);
        $Sequence->update($request->all());
 
        return response()->json('The Sequence successfully updated');
    }
 
    // delete Sequence
    public function delete($id)
    {
        $Sequence = Sequence::find($id);

        //delete all steps in sequence
        DB::table('steps')->where('sequence_id', $id)->delete();

        $Sequence->delete();
 
        return response()->json('The Sequence successfully deleted');
    }
}
