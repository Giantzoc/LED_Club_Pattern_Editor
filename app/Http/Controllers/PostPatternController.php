<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PostPatternController extends Controller
{
    // add post
    
    public function add(Request $request)
    {        
        Log::debug($request->getContent());
        $steps = $request->getContent();
        
        // $post = new Post([
        //     'title' => $request->input('title'),
        //     'description' => $request->input('description')
        // ]);
        // $post->save();
 
        return response()->json('The post successfully added');
    }
}
