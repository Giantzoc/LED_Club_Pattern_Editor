<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sequence;
use App\Step;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StepsController extends Controller
{
    // update Sequence
    public function store($id, Request $request)
    {
        Log::debug($request->all());
        $sequence = Sequence::find($id);
        $oldSteps = $sequence->steps()->get();

        //delete all steps
        DB::table('steps')->where('sequence_id', $id)->delete();

        $newSteps = $request->all();
        foreach($newSteps as &$step){
            $step['created_at'] = null;
            $step['updated_at'] = null;
            //$newSteps[] = $step;
        };

        //insert all steps from the request
        Step::insert($newSteps);
        return response()->json('The Sequence successfully updated');
    }
}
