<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Pattern;
use Illuminate\Support\Facades\Log;
 
class PatternController extends Controller
{
    // all Patterns
    public function index()
    {
        $Patterns = Pattern::all()->sortByDesc('id')->toArray();
        return array_reverse($Patterns);
    }
 
    // add Pattern
    public function add(Request $request)
    {        
        Log::debug($request);
        $Pattern = new Pattern([
            'id' => $request->input('id'),
            'name' => $request->input('name'),
        ]);
        $Pattern->save();
 
        return response()->json('The Pattern successfully added');
    }
 
    // edit Pattern
    public function edit($id)
    {
        $Pattern = Pattern::find($id);
        return response()->json($Pattern);
    }
 
    // update Pattern
    public function update($id, Request $request)
    {
        $Pattern = Pattern::find($id);
        $Pattern->update($request->all());
 
        return response()->json('The Pattern successfully updated');
    }
 
    // delete Pattern
    public function delete($id)
    {
        $Pattern = Pattern::find($id);
        $Pattern->delete();
 
        return response()->json('The Pattern successfully deleted');
    }
}