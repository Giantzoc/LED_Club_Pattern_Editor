<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Sequence;
use App\Step;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Type\Hexadecimal;

class FileController extends Controller
{

    public function index(){
        $steps = Step::orderBy('sequence_id', 'ASC')->orderby('time', 'ASC')->get();
        
        $file = "// SequenceData.h file created " .  date("Y/m/d") .
"// Sequence spreadsheet name is The Force Theme.
        
#ifndef SequenceData_h
#define SequenceData_h
        
#include Arduino.h
        
// Array with all the pre-defined sequence data
// [0-1]   Sequence ID (16 bits)
// [2-5]   Start Time in milliseconds (32 bits)
// [6]     Pattern ID
// [7]     Direction (0 = Reverse, 1 = Forward)
// [8-9]   Type (16 bits)
// [10]    Tempo in beats per minute
// [11-13] Color1 in RGB values
// [14-16] Color2 in RGB values
static const uint8_t sequenceData[][17] PROGMEM = { \n";
//$file .= "{ 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 2, 1, 0x00, 0x00, 143, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55 }, // M1, B1, Club ID";

        foreach($steps as $step){
            $hex_sequence = $this->lpad(4, "0", dechex($step->sequence_id));
            $file .= "{ 0x" . substr($hex_sequence, 0, 2) . ","; //0
            $file .= " 0x" . substr($hex_sequence, 2, 2) . ","; //1

            $hex_time = $this->lpad(8, "0", dechex($step->time));
            $file .= " 0x" . substr($hex_time, 0, 2) . ","; //2
            $file .= " 0x" . substr($hex_time, 2, 2) . ","; //3
            $file .= " 0x" . substr($hex_time, 4, 2) . ","; //4
            $file .= " 0x" . substr($hex_time, 6, 2) . ","; //5

            $file .= " " . $step->pattern_id . ","; //6
            if($step->direction){
                $file .= " 1,"; //7
            } else{
                $file .= " 0,"; //7
            }

            $hex_type = $this->lpad(4, "0", dechex($step->type));
            $file .= " 0x" . substr($hex_type, 0, 2) . ","; //8
            $file .= " 0x" . substr($hex_type, 2, 2) . ","; //9

            $file .= " " . $step->tempo . ","; //10

            $file .= " 0x" . substr($step->color1, 1, 2) . ","; //11
            $file .= " 0x" . substr($step->color1, 3, 2) . ","; //12
            $file .= " 0x" . substr($step->color1, 5, 2) . ","; //13

            $file .= " 0x" . substr($step->color2, 1, 2) . ","; //14
            $file .= " 0x" . substr($step->color2, 3, 2) . ","; //15
            $file .= " 0x" . substr($step->color2, 5, 2) . " },\n"; //16            
        };

        $file .= "};

#endif /* SequenceData_h */";

        $fileName = 'export.h';
        Storage::put($fileName, $file);
        return Storage::download($fileName); //, $name, $headers
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'file' => ['required', 'file']
        ]);
        
        $file = request()->file('file')->get();
        
        $steps = explode('{', $file);
        $previousSequenceId = -1;
        $newSequence = null;

        foreach($steps as $step)
        {
            $array = explode(',', $step);
            foreach ($array as &$item) {
                $item = $this->trimValue($item);
            }
            
            if(count($array) > 16){
                $sequenceId = (int)(hexdec($array[0]) * 256 + hexdec($array[1]));
                if($previousSequenceId != $sequenceId){
                    $newSequence = Sequence::create([
                        'name' => "import " . $sequenceId,
                    ]);
                }

                $previousSequenceId = $sequenceId;
                $time = (int)(hexdec($array[2]) * 16777216 + hexdec($array[3]) * 65536  + hexdec($array[4]) * 256 + hexdec($array[5]));
                $patternId = (int)$array[6];
                $direction = (bool)$array[7];
                $type = hexdec($array[8] . $array[9]);
                $tempo = (int)$array[10];
                $color1 = $this->color($array[11] , (string)$array[12] , (string)$array[13]);
                $lastColor = explode('}', $array[16]);
                $color2 = $this->color((string)$array[14] ,(string)$array[15] ,(string)$this->trimValue($lastColor[0]));
                
                $step = Step::create([
                    'sequence_id' => $newSequence->id,
                    'time' => $time,
                    'pattern_id' => $patternId,
                    'direction' => $direction,
                    'type' => $type,
                    'tempo' => $tempo,
                    'color1' => $color1,
                    'color2' => $color2,
                ]);
            }
        }

        return response('', 200);
    }

    function trimValue($item){
        $trimmed = trim($item, " }");
        if(substr($trimmed, 0, 2) == "0x"){
            $item = substr($trimmed, 2, strlen($trimmed));
        }
        
        return $item; //cut out the 0x
    }

    private function color($string1, $string2, $string3){
        return "#" . $this->leadingZeroes($string1) . $this->leadingZeroes($string2) . $this->leadingZeroes($string3);
    }

    private function leadingZeroes($string){
        $string = '00' . $string;
        $string = substr($string, -2);
        return $string;
    }

    private function lpad($length, $character, $string){
        $tempstring = "";
        for($i = 0; $i < $length; $i++){
            $tempstring .= $character;
        }
        $string = $tempstring . $string;
        $string = substr($string, -(int)$length);
;
        return $string;
    }
}
