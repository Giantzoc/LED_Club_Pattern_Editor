<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $guarded = [];
    
    public function Pattern(){
        return $this->belongsTo(Pattern::class);
    }

    public function Sequence(){
        return $this->belongsTo(Sequence::class);
    }
}
