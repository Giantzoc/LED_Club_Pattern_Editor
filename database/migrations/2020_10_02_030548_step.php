<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Step extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('sequence_id');
            $table->unsignedInteger('pattern_id');
            $table->unsignedBigInteger('time');
            //$table->unsignedInteger('measure');
            //$table->unsignedInteger('beats_per_measure');
            //$table->unsignedInteger('beat');
            //$table->unsignedInteger('milliseconds');
            $table->boolean('direction');
            $table->unsignedInteger('type');
            $table->unsignedInteger('tempo');
            $table->string("color1", 7);
            $table->string("color2", 7);
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
