<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Pattern extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patterns')->insert(['id' => 0,'name' => 'PatternSequence']);
        DB::table('patterns')->insert(['id' => 1,'name' => 'BatteryCharge']);
        DB::table('patterns')->insert(['id' => 2,'name' => 'ClubID']);
        DB::table('patterns')->insert(['id' => 3,'name' => 'RainbowCycle']);
        DB::table('patterns')->insert(['id' => 4,'name' => 'ColorCycle']);
        DB::table('patterns')->insert(['id' => 5,'name' => 'TheaterChase']);
        DB::table('patterns')->insert(['id' => 6,'name' => 'Lightsaber']);
        DB::table('patterns')->insert(['id' => 7,'name' => 'TwoTone']);
        DB::table('patterns')->insert(['id' => 8,'name' => 'Scanner']);
        DB::table('patterns')->insert(['id' => 9,'name' => 'Flash4']);
        DB::table('patterns')->insert(['id' => 10,'name' => 'Flash3']);
        DB::table('patterns')->insert(['id' => 11,'name' => 'Fade']);
        DB::table('patterns')->insert(['id' => 12,'name' => 'Gravity']);
        DB::table('patterns')->insert(['id' => 13,'name' => 'FreeFall']);
    }

    public function createPattern($id, $name){
        
    }
}
