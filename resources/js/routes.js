import AllPatterns from './components/AllPatterns.vue';
import AddPattern from './components/AddPattern.vue';
import EditPattern from './components/EditPattern.vue';

import AllSequences from './components/AllSequences.vue';
import AddSequence from './components/AddSequence.vue';
import EditSequence from './components/EditSequence.vue';

import Import from './components/Import.vue';
 
export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllSequences
    },
    {
        name: 'AllPatterns',
        path: '/patterns',
        component: AllPatterns
    },
    {
        name: 'AddPattern',
        path: '/patterns/add',
        component: AddPattern
    },
    {
        name: 'EditPattern',
        path: '/patterns/edit/:id',
        component: EditPattern
    },
    
    {
        name: 'AllSequences',
        path: '/sequences',
        component: AllSequences
    },
    {
        name: 'AddSequence',
        path: '/sequences/add',
        component: AddSequence
    },
    {
        name: 'EditSequence',
        path: '/sequences/edit/:id',
        component: EditSequence
    },
    {
        name: 'import',
        path: '/import',
        component: Import
    }
];