<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('pattern', 'PatternController@index');

// Route::get('posts', 'PostController@index');
// Route::group(['prefix' => 'post'], function () {
//     Route::post('add', 'PostController@add');
//     Route::get('edit/{id}', 'PostController@edit');
//     Route::post('update/{id}', 'PostController@update');
//     Route::delete('delete/{id}', 'PostController@delete');
// });
Route::get('patterns', 'PatternController@index');

Route::post('patterns/add', 'PatternController@add');
Route::get('patterns/edit/{id}', 'PatternController@edit');
Route::post('patterns/update/{id}', 'PatternController@update');
Route::delete('patterns/delete/{id}', 'PatternController@delete');

Route::get('sequences', 'SequenceController@index');
Route::post('sequences/add', 'SequenceController@add');
Route::get('sequences/edit/{id}', 'SequenceController@edit');
Route::post('sequences/update/{id}', 'SequenceController@update');
Route::delete('sequences/delete/{id}', 'SequenceController@delete');

Route::get('download', 'FileController@index');
Route::post('import', 'FileController@store');
Route::post('steps/store/{id}', 'StepsController@store');